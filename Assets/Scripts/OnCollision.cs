﻿using UnityEngine;
using System.Collections;

public class OnCollision : MonoBehaviour {

	void OnCollisionEnter(Collision coll){
		if(coll.gameObject.tag == "Element"){
			Destroy(gameObject);
			Destroy(coll.gameObject);
		}
	}
}