﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	public GameObject[] Asteroids;
	public int yMax, yMin, xMax, xMin;
	private bool isSpawning = false;

	// Use this for initialization
	void Start () {

	}

	IEnumerator createAnAsteroid(GameObject[] asteroids, int x){
		GameObject thing = asteroids [Random.Range(0, asteroids.Length)];
		Instantiate (thing, new Vector3 (x, yMax, 0.0f), Quaternion.identity);
		yield return new WaitForSeconds (Random.Range (0, 2));

		isSpawning = false;

	}
	// Update is called once per frame
	void Update () { 
		if (!isSpawning){
			isSpawning = true;
			int x = Random.Range (xMin, xMax);
			StartCoroutine(createAnAsteroid(Asteroids, x));
		}
	}
}
